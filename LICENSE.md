All rights reserved by Jacob Hrbek <kreyren@fsfe.org> identified using a GPG identifier from OpenPGP.org keyserver <https://keys.openpgp.org> in 08/06/2021-EU 7:31:05 CEST

This license currently mimics GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html> with option to be changed at any time